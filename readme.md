# Bravomedia TinyMCE Widget

Requires [Black Studio TinyMCE Widget](https://sv.wordpress.org/plugins/black-studio-tinymce-widget/)

## Usage

### Required, add classname to widget
```php
add_filter('black_studio_tinymce_before_text', function($html, $instance) {
    if(!empty($instance['custom_class'])) {
        $html = str_replace('textwidget', 'textwidget ' . esc_attr($instance['custom_class']));
    }
    return $html;
});
```

### Optional, add more options
```php
add_filter('bravo_tinymce_widget_options', function($options) {
    $options['my_custom_option'] = array('label' => 'Custom option', 'default' => 'Default value');
    return $options;
});
```
