<?php

/**
 * The plugin bootstrap file
 *
 * @link              http://www.bravomedia.se/
 * @since             1.0.0
 * @package           bravo-tinymce-widget
 *
 * @wordpress-plugin
 * Plugin Name:       Bravomedia TinyMCE Widget
 * Plugin URI:        http://www.bravomedia.se/
 * Description:       Black Studo TinyMCE Widget with extra options
 * Version:           1.0.0
 * Author:            Bravomedia
 * Author URI:        http://www.bravomedia.se/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bravo-tinymce-widget
 * Depends:           Black Studio TinyMCE Widget
 */

if(!defined('WPINC')) {
    die;
}

if(!class_exists('Bravo_TinyMCE_Widget') && class_exists('WP_Widget_Black_Studio_TinyMCE')) {

    /**
     * Bravo_TinyMCE_Widget
     * 
     * @todo Editor is broken the first time it's added, saving or reloading fixes that.
     * 
     */
    class Bravo_TinyMCE_Widget extends WP_Widget_Black_Studio_TinyMCE {
        public $bravo_default_options = array(
            'custom_class' => array('label' => 'Class:', 'default' => ''),
        );

        public function __construct() {
            /* translators: title of the widget */
            $widget_title = __('Bravomedia Visual Editor', 'bravo-tinymce-widget');
            /* translators: description of the widget, shown in available widgets */
            $widget_description = __('Arbitrary text or HTML with visual editor', 'bravo-tinymce-widget');
            $widget_ops = array('classname' => 'widget_black_studio_tinymce', 'description' => $widget_description);
            $control_ops = array('width' => 800, 'height' => 600);
            // Note: Calling WP_Widget constructor not parent, also do not remove the "black-studio-tinymce" prefix.
            WP_Widget::__construct('black-studio-tinymce-' . get_class($this), $widget_title, $widget_ops, $control_ops );
        }

        public function bravo_options() {
            $options = apply_filters('bravo_tinymce_widget_options', $this->bravo_default_options, $this);
            return $options;
        }

        public function update($new_instance, $old_instance) {
            $instance = parent::update($new_instance, $old_instance);
            foreach($this->bravo_options() as $key => $option) {
                $instance[$key] = isset($new_instance[$key]) ? strip_tags($new_instance[$key]) : $option['default'];
            }
            return $instance;
        }

        public function form($instance) {
            foreach($this->bravo_options() as $key => $option) {
                $class = isset($instance[$key]) ? $instance[$key] : $option['default'];
                ?>
                    <p>
                        <label for="<?php echo esc_attr($this->get_field_id($key)); ?>"><?php _e($option['label']); ?></label>
                        <input
                            class="widefat"
                            id="<?php echo esc_attr($this->get_field_id($key)); ?>"
                            name="<?php echo esc_attr($this->get_field_name($key)); ?>"
                            type="text"
                            value="<?php echo esc_attr($class); ?>"
                        />
                    </p>
                <?php
            }
            parent::form($instance);
        }
    }

    add_action('widgets_init', function() {
        register_widget('Bravo_TinyMCE_Widget');
    });
}
